import tomato as tt
from tomato.functions import utils
from tomato_rules import game_of_life as rule

cell_size = 4
dimensions = 100

state_matrix = utils.random_matrix(dimensions, 2)

board = tt.Board(rule, cell_size=cell_size)
board.start(state_matrix)
