import tomato as tt
from tomato.functions import utils
from tomato_rules import high_life as rule

cell_size = 4
dimensions = (150, 100)

state_matrix = utils.random_matrix(dimensions, (0, 1))

board = tt.Board(rule, cell_size=cell_size)
board.start(state_matrix)
