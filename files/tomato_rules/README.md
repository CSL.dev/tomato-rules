# Rulesets

Before you share your ruleset, we ask you to provide authorship
information (optional), such as your name and/or code sharing
website profile (codeberg, for instance), and a short description of
the rule in the form of a docstring. For example, check out
`game_of_live.py`:
```
from tomato.classes import cell

"""
Author: Eduardo Lopes Dias (codeberg.org/eduardotogpi)

An implementation of Conways' classic Game of Life. Besides being cool to
look at, it serves as a simple example as to how new rulesets can be
implemented.
"""


class Cell(cell.CellTemplate):
--snip--
```
If an appropriate description is too long to reasonably include in
the file itself, please provide links or references to where it
is properly explained.

Whenever possible, avoid using packages that are not required or
provided by tomato-engine or tomato-rules. If that can't be
helped, that's fine too.

Finally, please note that **every file in this repository is
licensed under the GPLv3+ unless stated otherwise**. So if you
would prefer to share your ruleset under a different license, you
must explicitly state that in any file you share.
