import numpy as np
from numpy import random
from tomato.classes import cell

""" Author: Luiza Licarião
Fake news dissemination model. This model features 6 types of cells, whose parameters
can be modified through cell_args:
Neutral (N)
    color: sandy brown, 
    value: 0,
    explanation: People who haven't heard of the news or formed an opinion yet.

Believer (B)
    color: light sky blue,
    value: 1,
    explanation: People who believe in the disinformation but aren't actively trying to
    disseminate it.

Disseminating Believer (DB)
    color: dark gray, 
    value: 2,
    ideia: People who believe in the disinformation and are actively trying to
    disseminate it.

Fact Checkers (FC)
    color: granny smith apple, 
    value: 3,
    explanation: A transitory state in which the person is deciding whether or not the
    news is true.

Skeptic (S)
    color: light pink, 
    value: 4,
    explanation: People who don't believe in the disinformation but aren't actively
    promoting skepticism to other people.

Disseminating Skeptic (DS)
    color: khaki, 
    value: 5,
    explanation: People who don't believe in the disinformation and are actively
    trying to convince other people of its falsehood.

Each cell receives 4 parameters through cell_args. The first is a matrix with the same
dimensions as state_matrix and whose values are between 0 and 1, and represent each
individual's (that is, each cell) intrinsic propensity to believe in the information
(which we call the individual's "credulity") regardless of its neighborhood. The second
is a global "alpha" value which represents the degree of "peer pressure", that is, how
strong is the cell's preference for the beliefs of its neighbors. The third is the seed
for the RNG and the fourth is now many random distant neighbors each cell is going to
consider, aside from its regular Moore neighbors.

"""

class FNCell(cell.CellTemplate):
    # {{{
    colors = (
        (244, 164, 96),  # N "sandy brown" 0
        (135, 206, 250),  # B "light sky blue" 1
        (169, 169, 169),  # DB "dark gray" 2
        (144, 238, 144),  # FC "granny smith apple" 3
        (255, 182, 193),  # S "light pink" 4
        (240, 230, 140),  # DS "khaki" 5
    )

    def __init__(self, val, pos, cell_args):
        self.lin, self.col = pos
        self.value = val

        self.prob = cell_args[0][self.lin, self.col]  # the individual's credulity
        self.alpha = cell_args[1]  # preference for immediate neighbors

        FNCell.rng = np.random.default_rng(cell_args[2])
        FNCell.random_neighbors_num = cell_args[3]

    def update(self, state_matrix):
        self.state_matrix = state_matrix

        if self.value == 0:
            if self.believing_ratio >= 0.5:
                self.value = 1
            elif self.believing_ratio < 0.5 and self.believing_ratio >= 0.1:
                if random.rand() > 0.7:
                    self.value = 3
                else:
                    self.value = 0
            else:
                self.value = 4

        elif self.value == 1:
            if self.believing_ratio >= 0.5:
                if random.rand() > 0.7:
                    self.value = 2
                else:
                    self.value = 1
            elif self.believing_ratio < 0.5 and self.believing_ratio >= 0.3:
                self.value = 1
            else:
                self.value = 3

        elif self.value == 3:
            if self.believing_ratio >= 0.8:
                self.value = 1
            elif self.believing_ratio >= 0.6 and self.believing_ratio < 0.8:
                self.value = 0
            else:
                if random.rand() > 0.5:
                    self.value = 4
                else:
                    self.value = 3

        elif self.value == 4:
            if self.believing_ratio <= 0.3:
                if random.rand() > 0.8:
                    self.value = 5
                else:
                    self.value = 4
            elif self.believing_ratio > 0.6:
                self.value = 3
            else:
                self.value = 4

    @property
    def neighbors(self):
        return self.moore_neighborhood

    @property
    # Retorna um número n de vizinhos aleatórios pelo tabulero
    def rand_neighbors(self):
        return FNCell.rng.choice(self.state_matrix.flatten(), FNCell.random_neighbors_num)

    @property
    # Retorna o valor que será utilizado para determinar a mudança de estado
    def believing_ratio(self):
        B = self.neighbors.count(1) + list(self.rand_neighbors).count(1)
        BD = self.neighbors.count(2) + list(self.rand_neighbors).count(2)
        SD = self.neighbors.count(5) + list(self.rand_neighbors).count(5)
        S = self.neighbors.count(4) + list(self.rand_neighbors).count(4)
        P = self.prob
        alfa = self.alpha
        viz = (alfa / (8)) * (2 * BD + B - 2 * SD)
        prob = (1 - alfa) * P
        if viz >= 0:
            return viz + prob
        else:
            return prob

    @staticmethod
    def display(value):
        return FNCell.colors[value]

    @staticmethod
    def from_display(value):
        return FNCell.colors.index(tuple(value))


# }}}
