from tomato.classes import cell

"""
Author: Eduardo Lopes Dias (codeberg.org/eduardotogpi)
Modified from Murilo's Langton's Ant

Langton's ant, except that now it shows the ant as an almost imperceptible
red dot, at a great cost in performance.
"""


class Cell(cell.CellTemplate):
    # {{{
    def __init__(self, val, pos, cell_args):

        global ant_pos
        global ant_dir

        self.value = val
        self.lin, self.col = pos

        ant_pos = cell_args["pos"]
        ant_dir = cell_args["dir"]

    def update(self, state_matrix):

        global ant_pos
        global ant_dir

        if ant_pos == self.pos:
            if self.value == 0:
                self.value = 2
                ant_dir = (
                    -ant_dir[1],
                    ant_dir[0],
                )
            elif self.value == 1:
                self.value = 3
                ant_dir = (
                    ant_dir[1],
                    -ant_dir[0],
                )

            m, n = state_matrix.shape
            ant_pos = (
                (ant_dir[0] + ant_pos[0]) % m,
                (ant_dir[1] + ant_pos[1]) % n,
            )
        else:
            if self.value == 2:
                self.value = 1
            elif self.value == 3:
                self.value = 0

    @staticmethod
    def display(value):
        if value == 0:
            return (0, 0, 0)
        elif value == 1:
            return (255, 255, 255)
        else:
            return (255, 0, 0)


# }}}
