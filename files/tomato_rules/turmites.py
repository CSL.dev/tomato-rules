from tomato.classes import cell

"""
Author: Eduardo Lopes Dias (codeberg.org/eduardotogpi)

A generalization of Langton's ant.
"""

class Ant(cell.CellTemplate):
    # {{{

    # fmt: off
    colors = (
        (0, 0, 0),          # Black
        (255, 255, 255),    # White
        (255, 0, 0),        # Red
        (0, 255, 0),        # Green
        (0, 0, 255),        # Blue
        (255, 0, 255),      # Pink
        (255, 255, 0),      # Yellow
        (0, 255, 255),      # Cyan
    )
    # fmt: on

    def __init__(self, val, pos, cell_args):

        global ant_pos
        global ant_dir

        self.value = val
        self.lin, self.col = pos

        ant_pos = cell_args["pos"]
        ant_dir = cell_args["dir"]
        Ant.name = cell_args["name"]
        Ant.colors = cell_args.get("colors", Ant.colors)

        if cell_args.get("loop_colors", False):
            Ant.display = Ant.display_looping


    def update(self, state_matrix):

        global ant_pos
        global ant_dir

        if ant_pos == self.pos:
            self.value = (self.value + 1) % len(Ant.name)

            if Ant.name[self.value] == 'R':
                ant_dir = (
                    -ant_dir[1],
                    ant_dir[0],
                )
            elif Ant.name[self.value] == 'L':
                ant_dir = (
                    ant_dir[1],
                    -ant_dir[0],
                )

            # Wrap around the board when close to its edge
            m, n = state_matrix.shape
            ant_pos = (
                (ant_dir[0] + ant_pos[0]) % m,
                (ant_dir[1] + ant_pos[1]) % n,
            )

    @staticmethod
    def display(value):
        return Ant.colors[value]

    @staticmethod
    def display_looping(value):
        # Wraps around the colors tuple. Note that it makes the
        # simulation slower and loading from a png will not work
        # as expected.
        return Ant.colors[value % len(Ant.colors)]

    @staticmethod
    def from_display(value):
        return Ant.colors.index(tuple(value))

# }}}
