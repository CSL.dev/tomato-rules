import numpy as np
import tomato as tt

from tomato.classes import cell

class OpinionDynamics(cell.CellTemplate):

    # Parâmetro que diz quanto os indivíduos são influenciados pela opinião dos
    # outros
    mu = 0.6

    def __init__(self, val, pos):
        self.lin, self.col = pos
        self.value = val
        self.opi = self.value.real
        self.inc = self.value.imag

    def update(self, state_matrix):
        self.state_matrix = state_matrix

        self.selected_neighbor = self.rand_neighbors
        self.neighbor_opi = self.selected_neighbor.real
        self.neighbor_inc = self.selected_neighbor.imag

        if self.agreement_relative > self.neighbor_inc:


            self.inc += OpinionDynamics.mu * (
                (self.agreement_relative / self.neighbor_inc) - 1
            ) * (self.neighbor_inc - self.inc)

            
            self.opi += OpinionDynamics.mu * (
                (self.agreement_relative / self.neighbor_inc) - 1
            ) * (self.neighbor_opi - self.opi)
            
            
            self.value = self.opi + 1.0j * self.inc
            

    # Retorna uma célula qualquer, aleatoriamente
    @property
    def rand_neighbors(self):
        lines, cols = self.state_matrix.shape
        rand_lin = np.random.randint(lines)
        rand_col = np.random.choice(cols)
        v_neighbor = self.state_matrix[rand_lin, rand_col]

        return v_neighbor

    # É tipo a diferença das incertezas das células
    @property
    def agreement_relative(self):

        SX = self.value.real
        SU = self.value.imag
        NX = self.neighbor_opi
        NU = self.neighbor_inc
        h = min(NX + NU, SX + SU) - max(NX - NU, SX - SU)

        return h

    @staticmethod
    def display(value):
        # Esquerdistas 
        opi_color = int(255 * (value.real + 1) / 2)
        inc_color = int(((value.imag) / 2) * 255)
        return (255 - opi_color, inc_color, opi_color)
