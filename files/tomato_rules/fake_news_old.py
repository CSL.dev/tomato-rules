import numpy as np
from numpy import random
from tomato.classes import cell

""" Author: Luiza Licarião
Modelo de disseminação de Fake News. Este modelo conta com 6 tipos de células, 
cujos parâmetros podem ser alterados por meio dos cell_args:
1. Neutros (N){
        cor: laranja, 
        valor: 0,
        ideia: São pessoas que não possuem opinião formada sobre a notícia ou ainda não
        tiveram contato com a mesma. }
2. Crentes (B){
        cor: azul,
        valor: 1,
        ideia: Pessoas que acreditam na notícia mas não possuem um grau de esforço
        alto para disseminá-la. }
3. Crentes disseminadores (DB){
        cor: cinza, 
        valor: 2,
        ideia: Pessoas que acreditam na notícia e se esforçam para disseminá-la. }
4. Fact Checkers (FC){
        cor: verde, 
        valor: 3,
        ideia: É um estado de transição no qual as pessoas checam os fatos antes de
        formarem opinião sobre a notícia. }
5. Céticos (S): 
        cor: rosa, 
        valor: 4,
        ideia: Pessoas que não acreditam na notícia, mas não se esforçam para convencer
        os outros que a notícia é falsa.
6. Céticos disseminadores (DS){
        cor: amarelo, 
        valor: 5,
        ideia: Pessoas que não acreditam na notícia e se esforçam para convencer
        os outros que a notícia é falsa. }
        
Toda célula recebe 4 parâmetros no cell_args, o primeiro é um número aleatório entre 0 e
1 que representa a probabildade intrínseca do indíviduo acreditar na notícia sem
considerar sua vizinhança. O segundo é o valor de alfa que será usado na definição da
regra de transição e diz quão importante é para a sociedade a influência da vizinhança.
O terceiro é um valor de seed que permite a reprodutibilidade do programa. E o último é
o número de vizinhos aleatórios que se deseja ter no modelo.
"""

class Cell(cell.CellTemplate):
    # {{{
    colors = (
        (244, 164, 96),  # N laranja 0
        (135, 206, 250),  # B azul 1
        (169, 169, 169),  # BD cinza 2
        (144, 238, 144),  # FC verde 3
        (255, 182, 193),  # S rosa 4
        (240, 230, 140),  # SD amarelo 5
    )

    def __init__(self, val, pos, cell_args):
        self.lin, self.col = pos
        self.value = val

        self.prob = cell_args[0][self.lin, self.col]  # credulidade do indivíduo
        self.alpha = cell_args[1]  # preferência por vizinhos imediatos

        Cell.rng = np.random.default_rng(cell_args[2])
        Cell.random_neighbors_num = cell_args[3]

    def update(self, state_matrix):
        self.state_matrix = state_matrix

        if self.value == 0:
            if self.believing_ratio >= 0.5:
                self.value = 1
            elif self.believing_ratio < 0.5 and self.believing_ratio >= 0.1:
                if random.rand() > 0.7:
                    self.value = 3
                else:
                    self.value = 0
            else:
                self.value = 4

        elif self.value == 1:
            if self.believing_ratio >= 0.5:
                if random.rand() > 0.7:
                    self.value = 2
                else:
                    self.value = 1
            elif self.believing_ratio < 0.5 and self.believing_ratio >= 0.3:
                self.value = 1
            else:
                self.value = 3

        elif self.value == 3:
            if self.believing_ratio >= 0.8:
                self.value = 1
            elif self.believing_ratio >= 0.6 and self.believing_ratio < 0.8:
                self.value = 0
            else:
                if random.rand() > 0.5:
                    self.value = 4
                else:
                    self.value = 3

        elif self.value == 4:
            if self.believing_ratio <= 0.3:
                if random.rand() > 0.8:
                    self.value = 5
                else:
                    self.value = 4
            elif self.believing_ratio > 0.6:
                self.value = 3
            else:
                self.value = 4

    @property
    def neighbors(self):
        return self.moore_neighborhood

    @property
    # Retorna um número n de vizinhos aleatórios pelo tabulero
    def rand_neighbors(self):
        return Cell.rng.choice(self.state_matrix.flatten(), Cell.random_neighbors_num)

    @property
    # Retorna o valor que será utilizado para determinar a mudança de estado
    def believing_ratio(self):
        B = self.neighbors.count(1) + list(self.rand_neighbors).count(1)
        BD = self.neighbors.count(2) + list(self.rand_neighbors).count(2)
        SD = self.neighbors.count(5) + list(self.rand_neighbors).count(5)
        S = self.neighbors.count(4) + list(self.rand_neighbors).count(4)
        P = self.prob
        alfa = self.alpha
        viz = (alfa / (8)) * (2 * BD + B - 2 * SD)
        prob = (1 - alfa) * P
        if viz >= 0:
            return viz + prob
        else:
            return prob

    @staticmethod
    def display(value):
        return Cell.colors[value]

    @staticmethod
    def from_display(value):
        return Cell.colors.index(tuple(value))


# }}}
