from tomato.classes import cell

"""
Author: Eduardo Lopes Dias (codeberg.org/eduardotogpi)

Basically Conway's Game of Life, except that a dead cell can also become
alive if it is surrounded by 6 or 8 live cells. It forms some patterns not
possible on the Game of Life.
"""


class Cell(cell.CellTemplate):
    # {{{
    # Este exemplo é particularmente simples porque, francamente,
    # o CellTemplate foi feito pensando nele.

    def update(self, state_matrix):
        self.state_matrix = state_matrix

        # Célula morta:
        if self.value == 0:
            if self.live_neighbors in (3, 6, 8):
                self.value = 1
            else:
                self.value = 0
        # Célula viva:
        else:
            if self.live_neighbors in (2, 3):
                self.value = 1
            else:
                self.value = 0

    @property
    def neighbors(self):
        return self.moore_neighborhood

    @property
    def live_neighbors(self):
        return sum(self.neighbors)


# }}}
