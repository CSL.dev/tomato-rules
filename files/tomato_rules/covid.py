import math
import random

import numpy as np
from tomato.classes import cell


class CovidCell(cell.CellTemplate):  # modelo SAIRM
    def __init__(self, val, pos, cell_args):
        self.lin, self.col = pos
        self.value = val

        self.choices = np.random.choice(6)

    @classmethod
    def simulation_start(cls, state_matrix, cell_args):

        # Defining initial parameters
        cls.k1 = cell_args["k1"]
        cls.k2 = cell_args["k2"]
        cls.Pc1 = cell_args["Pc1"]
        cls.Pc2 = cell_args["Pc2"]
        cls.Pb1 = cell_args["Pb1"]
        cls.Pb2 = cell_args["Pb2"]
        cls.Pd = cell_args["Pd"]
        cls.Px = cell_args["Px"]

    def update(self, state_matrix):
        self.state_matrix = state_matrix

        # Qualquer célula pode morrer com prob. Pd, por uma causa qualquer
        num_morrer_whatever = random.random()
        if num_morrer_whatever < CovidCell.Pd:
            self.value = 4  # morre

        # Suscetíveis
        if self.value == 0:  # sucetíveis
            v1 = self.num_assintomaticos
            v2 = self.num_infectados

            # Equação que define a prob. de pegar covid
            Pi = 1 - (math.exp(-(CovidCell.k1 * v1 + CovidCell.k2 * v2)))

            # Isso vai dizer se ele pega covid
            num_pegar = random.random()
            if num_pegar < Pi:

                # Isso vai dizer se ele fica assintomatico ou não
                num_estado = random.random()
                if num_estado < CovidCell.Px:
                    self.value = 2  # assintomatico
                else:
                    self.value = 1  # infectado

        # Assintomático
        elif self.value == 2:

            # Número que dirá se ele vai morrer ou recuperar
            num_morrer = random.random()

            # Pc1 < Pb1, então ele vem primeiro nos ifs. Mudar a ordem
            # se Pb1 < Pc1
            if num_morrer < CovidCell.Pc1:
                self.value = 4  # morto
            if random.random() < CovidCell.Pb1:
                self.value = 3  # recuperado

        # Infectado
        elif self.value == 1:

            # Número que dirá se ele vai morrer ou recuperar
            num_morrer = random.random()

            # Pc2 < Pb2, então ele vem primeiro nos ifs. Mudar a ordem
            # se Pb2 < Pc2
            if num_morrer < CovidCell.Pc2:
                self.value = 4
            elif num_morrer < CovidCell.Pb2:
                self.value = 3

        # célula recuperada. ela pode morrer com prob. Pd
        elif self.value == 3:
            num_morrer = random.random()
            if num_morrer < CovidCell.Pd:
                self.value = 4  # morre

    @property
    def neighbors(self):
        return np.array(
            self.moore_neighborhood
        )

    @property
    def num_sucetiveis(self):  # supondo que suscetiveis = 0
        return np.count_nonzero(self.neighbors == 0)

    @property
    def num_infectados(self):  # supondo que infectados = 1
        return np.count_nonzero(self.neighbors == 1)

    @property
    def num_assintomaticos(self):  # supondo que assintomaticos = 2
        return np.count_nonzero(self.neighbors == 2)

    @property
    def num_recuperados(self):  # supondo que recuperados = 3
        return np.count_nonzero(self.neighbors == 3)

    @property
    def num_mortos(self):  # supondo que mortos = 4
        return np.count_nonzero(self.neighbors == 4)

    @staticmethod
    def display(value):
        if value == 0:  # suscetíveis
            return (0, 255, 0)  # verde
        elif value == 2:  # assintomáticos
            return (255, 0, 255)  # magenta
        elif value == 1:  # infectados
            return (255, 0, 0)  # vermelho
        elif value == 3:  # recuperados
            return (255, 255, 255)  # branco
        elif value == 4:  # mortos
            return (0, 0, 0)  # preto
