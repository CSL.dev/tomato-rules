import numpy as np
from tomato.classes import cell

"""
Author: Eduardo Lopes Dias (codeberg.org/eduardotogpi)

I accidentally created this rule while trying to improve another. This rule likes to
form amoeba-like structures that grow with a chaotic "outer membrane" and a stable
"cytoplasm". There are essentially three types of membrane cells (valued 0, 1 and 2), a
cytoplasm (3) and the non-living cell (>3). 

I've tried coming up with biologically inspired explanations for this rule's behaviour
but I couldn't come up with anything, as this rule seems very arbitrary and intricate --
messing with the types and behaviours of the cells in order to make it more intuitive
always broke the rule for me.
"""


class Cell(cell.CellTemplate):
    # {{{

    # membrane: 0, 1, 2
    # cytoplasm: 3
    # "non-living": >3

    def update(self, state_matrix):
        self.state_matrix = state_matrix

        if self.value > 2:  # cytoplasm or non-living
            if 3 in self.membrane_neighbors:
                self.value = self.membrane_neighbors.index(3)
        else:   # membrane
            if 3 in self.membrane_neighbors:
                self.value = self.membrane_neighbors.index(3)
            elif 2 in self.membrane_neighbors:
                self.value = self.membrane_neighbors.index(2)
            else:
                self.value = 3


    @property
    def neighbors(self):
        return self.moore_neighborhood

    @property
    def membrane_neighbors(self):
        # Returns the number of each cell type in a Moore neighborhood
        return (
            self.neighbors.count(1),
            self.neighbors.count(2),
            self.neighbors.count(3),
        )

    @staticmethod
    def display(value):
        return (0, value*50 + 100, 150) if value <= 3 else (0, 0, 0)

    @staticmethod
    def from_display(value):
        if value[1] == 100:
            return 0
        elif value[1] == 150:
            return 1
        elif value[1] == 200:
            return 2
        elif value[1] == 250:
            return 3
        else:
            return 4    # the value here doesn't matter as long as it is >3
# }}}
