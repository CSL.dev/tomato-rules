from tomato.classes import cell

"""
Author: Luiza Licarião

Simple fake news dissemination model. This model features 3 types of cells:
1. Neutral (N)
    color: gray,
    value: 0,
    description: People who haven't formed an opinion about the news or haven't heard of
    it yet.

2. Believers (B)
    color: red,
    value: 1,
    description: People who believe in the news.

3. Skeptical (S):
    color: blue,
    value: -1,
    description: People who don't believe in the news.

The cells change their states based only on their Moore neighborhoods. Skeptical and
Believer cells change their values to Neutral if they have 5 or 6 opposing neighbors,
and go straight to the opposing value if they have 7 or 8 opposing neighbors. Neutral
cells change their minds more easily: they require at least 5 believing neighbors to
become Believers and at least 6 to become Skeptical.
"""


class Cell(cell.CellTemplate):
    # {{{
    def update(self, state_matrix):
        # {{{
        self.state_matrix = state_matrix

        # Neutral cells
        if self.value == 0:
            if self.b_neighbors >= 5:
                self.value = 1
            elif self.s_neighbors >= 6:
                self.value = -1
            else:
                self.value = 0

        # Believer cells
        elif self.value == 1:
            if self.s_neighbors in (5, 6):
                self.value = 0
            elif self.s_neighbors >= 7:
                self.value = -1
            else:
                self.value = 1

        # Skeptical cells
        else:
            if self.b_neighbors in (5, 6):
                self.value = 0
            elif self.b_neighbors >= 7:
                self.value = 1
            else:
                self.value = -1

    # }}}

    @property
    def neighbors(self):
        return self.moore_neighborhood

    @property
    def b_neighbors(self):
        # {{{
        # Number of believing neighbors
        return self.neighbors.count(1)

    # }}}

    @property
    def s_neighbors(self):
        # {{{
        # Number of skeptical neighbors
        return self.neighbors.count(-1)

    # }}}

    @staticmethod
    def display(value):
        # {{{
        if value == 0:
            return (169, 169, 169)
        elif value == 1:
            return (139, 0, 0)
        else:
            return (70, 130, 180)

    # }}}

    @staticmethod
    def from_display(value):
        # {{{
        if (value == (169, 169, 169)).all():
            return 0
        elif (value == (139, 0, 0)).all():
            return 1
        else:
            return -1


# }}}

# }}}
