from tomato.classes import cell

def conversion_condition(num_neighbors):
    # return num_neighbors > 2 # Espirais
    return num_neighbors == 3 # maze war 
    # return num_neighbors in (2, 3)  # Maze of chaos

def birth_condition(num_neighbors):
    return num_neighbors > 2


class ChaosMazeCell(cell.CellTemplate):
    # {{{

    # branco = 1
    # vermelho = 2
    # verde = 3
    # morto = 0 ou maior que 3

    def update(self, state_matrix):
        self.state_matrix = state_matrix

        if self.value == 3:
            if conversion_condition(self.live_red_neighbors):
                self.value = 2
            elif conversion_condition(self.live_green_neighbors):
                self.value = 3
            elif conversion_condition(self.live_white_neighbors):
                self.value = 1
        elif self.value == 1:
            if conversion_condition(self.live_green_neighbors):
                self.value = 3
            elif conversion_condition(self.live_white_neighbors):
                self.value = 1
            elif conversion_condition(self.live_red_neighbors):
                self.value = 2
        elif self.value == 2:
            if conversion_condition(self.live_white_neighbors):
                self.value = 1
            elif conversion_condition(self.live_red_neighbors):
                self.value = 2
            elif conversion_condition(self.live_green_neighbors):
                self.value = 3
        else:
            if birth_condition(self.live_white_neighbors):
                self.value = 1
            elif birth_condition(self.live_red_neighbors):
                self.value = 2
            elif birth_condition(self.live_green_neighbors):
                self.value = 3

    @property
    def neighbors(self):
        # Teste outras vizinhanças também!
        return self.moore_neighborhood

    @property
    def live_neighbors(self):
        return (
            self.live_white_neighbors,
            self.live_red_neighbors,
            self.live_green_neighbors,
        )

    @property
    def live_white_neighbors(self):
        return self.neighbors.count(1)

    @property
    def live_green_neighbors(self):
        return self.neighbors.count(3)

    @property
    def live_red_neighbors(self):
        return self.neighbors.count(2)

    @staticmethod
    def display(value):
        if value == 1:
            return (255, 255, 255)
        elif value == 2:
            return (255, 0, 0)
        elif value == 3:
            return (0, 255, 0)
        else:
            return (0, 0, 0)

    @staticmethod
    def from_display(value):
        if (value == (255, 255, 255)).all():
            return 1
        elif (value == (255, 0, 0)).all():
            return 2
        elif (value == (0, 255, 0)).all():
            return 3
        else:
            return 0


# }}}
