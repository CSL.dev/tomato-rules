import numpy as np
from tomato.classes import cell

# List of default colors for each automata, which are assigned in the order the automata
# are declared in the definitions dictionary
DEFAULT_COLORS = [
    (255, 255, 255),  # white
    (255, 0, 0),  # red
    (0, 255, 0),  # green
    (0, 0, 255),  # blue
    (255, 0, 255),  # magenta
    (255, 255, 0),  # yellow
    (0, 255, 255),  # cyan
    (125, 125, 125),  # grey
]

def definitions_to_arr(definitions):
    # {{{
    # Converts the definitions dict to two arrays for faster access

    rules_arr = np.zeros((len(definitions), 2, 13), dtype=np.int_) - 1
    colors_arr = np.zeros((len(definitions) + 1, 3), dtype=np.int_)
    colors_arr[0] = (0, 0, 0)
    neighborhoods_arr = np.zeros(len(definitions), dtype=np.str_)

    if isinstance(definitions, dict):
        for i, (key, val) in enumerate(definitions.items()):
            rules_arr[i], neighborhoods_arr[i] = parse_rulestring(key)
            colors_arr[i+1] = val
    elif isinstance(definitions, (tuple, list)):
        for i, val in enumerate(definitions):
            rules_arr[i], neighborhoods_arr[i] = parse_rulestring(val)
            colors_arr[i+1] = DEFAULT_COLORS[i]
    elif isinstance(definitions, str):
        rules_arr, neighborhood = parse_rulestring(definitions)
        rules_arr = np.array(rules_arr)
        neighborhoods_arr = np.array(neighborhood) 
        colors_arr = np.array([[0, 0, 0], [255, 255, 255]])

    return rules_arr, colors_arr, neighborhoods_arr


# }}}


def parse_rulestring(rulestring):
    # {{{
    rulestring = rulestring.upper()

    try:
        int(rulestring[-1])
        neighborhood = "M"
    except ValueError:
        neighborhood = rulestring[-1]
        rulestring = rulestring[:-1]

    b_str = rulestring[rulestring.find("B") : rulestring.find("/")]
    s_str = rulestring[rulestring.find("S") :]
    b_tuple = tuple(1 if str(i) in b_str else 0 for i in range(13))
    s_tuple = tuple(1 if str(i) in s_str else 0 for i in range(13))

    return np.array((b_tuple, s_tuple)), neighborhood


# }}}


class Cell(cell.CellTemplate):
    # {{{
    @classmethod
    def simulation_start(cls, state_matrix, definitions):
        # colors_arr is declared global to be used on the display and from_display
        # static methods
        global colors_arr

        # Converts the definitions dict to three arrays for faster access
        Cell.rules_arr, colors_arr, Cell.neighborhoods_arr = definitions_to_arr(
            definitions
        )

        Cell.num_rules = len(Cell.rules_arr)
        print(Cell.num_rules)
        print(colors_arr)

    def update(self, state_matrix):
# {{{
        self.state_matrix = state_matrix

        for n, rule_arr in enumerate(Cell.rules_arr):
            rule_val = n + 1

            b_arr, s_arr = rule_arr
            neighbors = self.neighbors(Cell.neighborhoods_arr[n])

            # Dead cell
            if self.value == 0:
                if b_arr[neighbors.count(rule_val)]:
                    self.value = rule_val
                    break
            # Live cell
            elif self.value == rule_val:

                next_rule_val = ((rule_val + 2) % Cell.num_rules) - 1
                prev_rule_val = ((rule_val - 2) % Cell.num_rules) + 1

                if next_rule_val == 0:
                    next_rule_val = 1
                if prev_rule_val == 0:
                    prev_rule_val = 1

                if b_arr[neighbors.count(prev_rule_val)]:
                    self.value = prev_rule_val
                elif not s_arr[neighbors.count(rule_val)]:
                    self.value = 0
                elif b_arr[neighbors.count(next_rule_val)]:
                    self.value = next_rule_val
# }}}

    def neighbors(self, nh_char):
# {{{
        if nh_char == "M":
            return self.moore_neighborhood
        elif nh_char == "V":
            return self.neumann_neighborhood
        elif nh_char == "L":
            return self.triangular_vertices_neighborhood
        elif nh_char == "LV":
            return self.triangular_1vertex_neighborhood
        elif nh_char == "LE":
            return self.triangular_edges_neighborhood
        elif nh_char == "H":
            return self.hexagonal_neighborhood
# }}}

    @staticmethod
    def display(val):
        global colors_arr

        return tuple(colors_arr[val])

    @staticmethod
    def from_display(val):
        global colors_arr

        for i, color in enumerate(colors_arr):
            if (color == val).all():
                return i

# }}}

definitions = {
    "B3/S23M": (255, 255, 255),
    "B3/S238M": (255, 0, 0),
    "B36/S23M": (0, 255, 0),
}
