from random import choice, random, seed

import numpy as np
from tomato.classes import cell

""" Author: Eduardo Lopes Dias (codeberg.org/eduardotogpi)# {{{

Modelo de propagação da dengue em zonas urbanas. Este modelo faz uso de números
complexos e conta com 6 tipos de células, cujos parâmetros podem ser alterados por meio
dos cell_args:
1. Mosquitos: cor: branca, valor: imaginário entre 0 e 3.99 (inclusivo)
    Andam aleatoriamente pelo tabuleiro. Tem tempo de vida dado por
    cell_args['mosquito_lifespan']. Quando passam por cima de uma casa, a infecta com
    probabilidade cell_args['infection_prob'], morrendo no processo caso
    cell_args['kamikaze_mosquito'] é True.

2. Casa: cor: verde a vermelho, valor: 2.0 (suscetível) a 3.0 (infectado)
    Quando infectada, morre com probabidade dada por cell_args['death_prob']. Caso
    sobreviva, se recupera em cell_args['heal_time'] gerações. Casas só podem ser
    infectadas quando suscetíveis (valor = 2.0, cor verde).

3. Água: cor: azul a ciano, valor: 4.0 (água 'mexida') a 5.0 (água parada)
    Quando parada, cria um mosquito com probabilidade cell_args['spawn_prob']. Difunde
    seu valor com outros vizinhos 'águas': valor = 0,75^(n. de vizinhos 'água') +
    0,25*(valor de cada vizinho) + cell_args['water_still_factor'].

4. Inspetor sanitário: cor: magenta, valor: imaginário entre 4.0 e 7.0 (inclusivo)
    Anda pelas ruas, virando sempre à esquerda. Mata os mosquitos em seu caminho.
    "Mexe" as águas em sua vizinhança.

5. Rua: cinza, valor: 7.0
    Não faz nada, é simplesmente as células sob as quais o inspetor pode andar.

6. Fundo: preto, valor: 8.0
    Não faz nada.
"""  # }}}


class Cell(cell.CellTemplate):
    # mosquito: x+0j ... x+3j -> white (real: on top of, complex: direction)
    # house: 2.0 ... 3.0 -> green (healthy) to red (infected)
    # water: 4.0 .. 5.0 -> blue (stirred) to cyan (still)
    # inspector: x+4j ... x+7j -> magenta (real: on top of, complex: direction)
    # street: 7.0 -> grey
    # background/walls: 8 -> black

    def __init__(self, val, pos, cell_args):
        # {{{
        global move

        self.value = val
        self.lin, self.col = pos

        seed(cell_args.get("seed", None))
        Cell.heal_time = cell_args.get("heal_time", 200)
        Cell.mosquito_lifespan = cell_args.get("mosquito_lifespan", 90)
        Cell.water_still_factor = cell_args.get("water_still_factor", 0.001)
        Cell.spawn_prob = cell_args.get("spawn_prob", 0.001)
        Cell.infection_prob = cell_args.get("infection_prob", 1)
        Cell.kamikaze_mosquito = cell_args.get("kamikaze_mosquito", False)
        Cell.death_prob = cell_args.get("death_prob", 0.0)

        move = True

    # }}}

    @classmethod
    def generation_start(cls, cell_args):
        # {{{
        # All this does is alternate the "move" flag at the start of each generation.
        global move

        move = not move

    # }}}

    def update(self, state_matrix):
        # {{{
        global move
        self.state_matrix = state_matrix

        if not move:
            if self.imagv < 8:  # a nosquito or an inspector
                if self.imagv < 4:  # mosquito
                    self.imagv = self.mosquito_next_step() + np.modf(self.imagv)[0]

                    self.imagv += 0.01
                    if (  # mosquito dies
                        np.around(100 * np.modf(self.imagv)[0], 3)
                        == Cell.mosquito_lifespan
                    ):
                        self.imagv = 8

                    # mosquito infects susceptible house
                    if np.isclose(self.value.real, 2.0):
                        if random() < Cell.infection_prob:
                            if random() < Cell.death_prob:  # house dies
                                self.realv = 8
                            else:
                                self.realv = 3.0
                            if Cell.kamikaze_mosquito:
                                self.imagv = 8

                else:  # inspector
                    self.imagv = self.inspector_next_step()

        else:
            if self.imagv < 8:  # a mosquito or an inspector
                self.imagv = 8

            if 2.0 < self.realv <= 3.0:
                # house slowly recovers
                self.realv = self.realv - 1 / Cell.heal_time
                if self.realv < 2.0:
                    self.realv = 2.0

            if 4.0 <= self.realv <= 5.0:
                # water diffuses its movement, gradually becoming still
                self.water_diffusion()
                if self.realv == 5.0:
                    if random() < Cell.spawn_prob:
                        self.imagv = choice((0, 1, 2, 3))

            for idx, val in enumerate(self.neighbors):
                # inspector in the neighborhood
                if 4 <= val.imag < 8:
                    if 4.0 <= self.realv <= 5.0:
                        self.realv = 4.0

                    # inspector will kill the mosquito if they land on the same cell
                    if idx == int(val.imag + 2) % 4:
                        self.imagv = val.imag
                        break
                elif val.imag < 4:
                    if idx == int(val.imag + 2) % 4:
                        self.imagv = val.imag

    # }}}

    # Display and from display{{{
    @staticmethod
    def display(value):
        # {{{
        if value.imag < 4:  # mosquito
            return (255, 255, 255)
        elif value.imag < 8:  # inspector
            return (255, 0, 255)
        elif value.real == 8.0:  # background
            return (0, 0, 0)
        elif value.real == 7.0:  # street
            return (128, 128, 128)
        elif 2.0 <= value.real <= 3.0:  # house
            scaled_value = (value.real - 2) * 255
            return (scaled_value, 255 - scaled_value, 0)
        elif 4.0 <= value.real <= 5.0:  # water
            scaled_value = (value.real - 4) * 255
            return (0, scaled_value, 255)

    # }}}

    @staticmethod
    def from_display(value):
        # {{{
        if (value == (0, 0, 0)).all():  # background
            return 8 + 8j
        elif (value == (128, 128, 128)).all():  # street
            return 7 + 8j
        elif (value == (255, 0, 255)).all():  # inspector
            return 7 + 4j
        elif (value == (255, 255, 255)).all():  # mosquito
            return 8 + choice((0, 1, 2, 3)) * 1j
        elif value[2] == 255:  # water
            return ((value[1] / 255) + 4) + 8j
        else:  # house
            return ((value[0] / 255) + 2) + 8j

    # }}}
    # }}}

    # Neighborhoods{{{
    @property
    def neighbors(self):
        # {{{
        #   0
        # 3 w 1
        #   2
        return self.neumann_neighborhood

    # }}}

    @property
    def second_neighbors(self):
        # {{{
        #      0
        #
        # 3    w    1
        #
        #      2
        lin, col = self.lin, self.col

        state_matrix = self.state_matrix
        m, n = state_matrix.shape
        lin, col = self.lin, self.col
        prev_lin, next_lin = lin - 2, lin + 2
        prev_col, next_col = col - 2, col + 2
        try:
            neighbors = [
                state_matrix[prev_lin, col],
                state_matrix[lin, next_col],
                state_matrix[next_lin, col],
                state_matrix[lin, prev_col],
            ]
        except IndexError:
            prev_lin %= m
            next_lin %= m
            prev_col %= n
            next_col %= n

            neighbors = [
                state_matrix[prev_lin, col],
                state_matrix[lin, next_col],
                state_matrix[next_lin, col],
                state_matrix[lin, prev_col],
            ]
        return neighbors

    # }}}

    @property
    def diagonal_neighbors(self):
        # {{{
        # 0   1
        #   w
        # 3   2
        lin, col = self.lin, self.col

        state_matrix = self.state_matrix
        m, n = state_matrix.shape
        lin, col = self.lin, self.col
        prev_lin, next_lin = lin - 1, lin + 1
        prev_col, next_col = col - 1, col + 1
        try:
            neighbors = [
                state_matrix[prev_lin, prev_col],
                state_matrix[prev_lin, next_col],
                state_matrix[next_lin, next_col],
                state_matrix[next_lin, prev_col],
            ]
        except IndexError:
            prev_lin %= m
            next_lin %= m
            prev_col %= n
            next_col %= n

            neighbors = [
                state_matrix[prev_lin, prev_col],
                state_matrix[prev_lin, next_col],
                state_matrix[next_lin, next_col],
                state_matrix[next_lin, prev_col],
            ]
        return neighbors

    # }}}
    # }}}

    # Convenience methods for complex number manipulation{{{
    @property
    def imagv(self):
        return self.value.imag

    @imagv.setter
    def imagv(self, val):
        self.value = self.value.real + val * 1j

    @property
    def realv(self):
        return self.value.real

    @realv.setter
    def realv(self, val):
        self.value = val + self.value.imag * 1j

    # }}}

    # 'Next step' functions{{{
    def mosquito_next_step(self):
        # {{{
        # This function chooses the next value (and therefore movement direction) for
        # the random walker. it has to take into account all cells within 1 and 2 units
        # of Neumann distance to make sure it won't choose a move that will result in it
        # landing on the same cell as another random walker.

        free_second_neighbors = set(
            x for x in range(4) if self.second_neighbors[x].imag >= 4
        )
        free_diag_neighbors = set(
            x
            for x in range(4)
            if self.diagonal_neighbors[x].imag >= 4
            and self.diagonal_neighbors[(x + 1) % 4].imag >= 4
        )
        free_neighbors = set(x for x in range(4) if self.neighbors[x].imag >= 4)
        try:
            chosen = choice(
                tuple(
                    free_neighbors.intersection(
                        free_second_neighbors, free_diag_neighbors
                    )
                )
            )
        except IndexError:
            # This will happen if the intersection of all "free_neighbors" sets is
            # empty, which means any move may result in it bonking another walker. In
            # this case it just chooses a random direction.
            return choice((0, 1, 2, 3))
        return chosen

    # }}}

    def inspector_next_step(self):
        # {{{
        # inspectors can only walk on roads, and always turn left at intersections

        if self.relative_neighbor(0).real != 7:
            for idx in range(4):
                if self.relative_neighbor(idx).real == 7:
                    return (self.imagv + idx) % 4 + 4
        elif self.relative_neighbor(3).real == 7:
            return (self.imagv + 3) % 4 + 4
        else:
            return self.imagv

    # }}}
    # }}}

    # Miscellaneous{{{
    def water_diffusion(self):
        # {{{
        water_neighbors = [
            0.25 * (x.real - 4) for x in self.neighbors if 4.0 <= x.real <= 5.0
        ]
        self.realv -= 4
        self.realv = np.power(0.75, len(water_neighbors)) * self.realv + sum(
            water_neighbors
        )
        self.realv = self.realv + 4 + Cell.water_still_factor
        if self.realv > 5.0:
            self.realv = 5.0

    # }}}

    def dir_to_index(self, direc):
        return int(self.imagv - 4 + direc) % 4

    def relative_neighbor(self, direc):
        # {{{
        # return the value of a neighbor found at a direction relative to the cell's
        # current direction
        # 0 - front; 1 - right; 2 - back; 3 - left
        return self.neighbors[self.dir_to_index(direc)]

    # }}}

    def relative_diag_neighbor(self, direc):
        # {{{
        # return the value of a diagonal neighbor found at a direction relative to the
        # cell's current direction
        # 0 - front-left; 1 - front-right; 2 - back-right; 3 - back-left
        return self.diagonal_neighbors[self.dir_to_index(direc)]

    # }}}


# }}}
