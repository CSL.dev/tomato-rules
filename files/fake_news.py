import numpy as np
import tomato as tt

from tomato_rules import fake_news as rule

cell_size = 8
board_dimensions = (80, 80)
seed = 5

np.random.seed(seed)
prob_matrix = np.random.rand(100, 100)

state_matrix = np.random.choice(
    a=[0, 1, 2, 3, 4, 5], size=board_dimensions, p=[0.7, 0.05, 0.1, 0.0, 0.05, 0.1]
)

board = tt.Board(rule, cell_size=cell_size)
board.start(state_matrix, cell_args=[prob_matrix, 0.5, seed, 4])
