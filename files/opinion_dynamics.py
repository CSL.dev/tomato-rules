import numpy as np
import tomato as tt

from tomato_rules import opinion_dynamics as rule

rng = np.random.default_rng(777)

size = (50, 50)
initial_opinions = rng.uniform(-0.5, 0.5, size=size)
initial_uncertanties = rng.uniform(0.8, 1.8, size=size)
initial_state_matrix = initial_opinions + 1.j * initial_uncertanties

radical_left = -1 + 1.0j * 0.8
radical_right = 1 + 1.0j * 0.8

num_leftists = 10
num_rightists = 10

r_left_lins = rng.integers(size[0], size=num_leftists)
r_left_cols = rng.integers(size[1], size=num_leftists)
initial_state_matrix[r_left_lins, r_left_cols] = radical_left

r_right_lins = rng.integers(size[0], size=num_rightists)
r_right_cols = rng.integers(size[1], size=num_rightists)
initial_state_matrix[r_right_lins, r_right_cols] = radical_right


cell_size = 8

board = tt.Board(rule, cell_size=cell_size)
board.start(initial_state_matrix, inline=False, generate_gif=False)
