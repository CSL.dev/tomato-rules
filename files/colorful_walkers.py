import numpy as np
import tomato as tt

from tomato_rules import colorful_walkers as rule

cell_size = 10
dimensions = (50, 50)
walker_list = [
    rule.WalkerTuple("B", (10, 10)),
    rule.WalkerTuple("G", (40, 10)),
    rule.WalkerTuple("R", (40, 40)),
    rule.WalkerTuple("Y", (10, 40)),
]

# can go diagonally
walker1_dirs = (
    (1, 0),
    (0, 1),
    (1, 1),
    (-1, 0),
    (0, -1),
    (1, -1),
    (-1, 1),
    (-1, -1),
)

walker1_weights = (1, 1, 1, 1, 1, 1, 1, 1)

# more likely to walk horizontally
walker2_dirs = (
    (1, 0),
    (-1, 0),
    (0, -1),
    (0, 1),
)

walker2_weights = (1, 1, 50, 50)

# may jump 2 cells at a time
walker3_dirs = (
    (1, 0),
    (0, 1),
    (-1, 0),
    (0, -1),
    (2, 0),
    (-2, 0),
    (0, -2),
    (0, 2),
)

walker3_weights = (1, 1, 1, 1, 1, 1, 1, 1)

# Knight in chess. Sometimes he does not move
walker4_dirs = (
    (0, 0),
    (2, 1),
    (-2, 1),
    (2, -1),
    (-2, -1),
    (1, 2),
    (-1, 2),
    (1, -2),
    (-1, -2),
)

walker4_weights = (100, 1, 1, 1, 1, 1, 1, 1, 1)


cell_args = {
    "possible_dirs": [walker1_dirs, walker2_dirs, walker3_dirs, walker4_dirs],
    "dir_weights": [
        walker1_weights,
        walker2_weights,
        walker3_weights,
        walker4_weights,
    ],
    "trail_color": "W",
    "gens_to_fade": 40,
}

state_matrix = np.zeros(dimensions)

board = tt.Board(rule, cell_size=cell_size)
board.start(
    state_matrix,
    walker_list,
    cell_args=cell_args,
)
