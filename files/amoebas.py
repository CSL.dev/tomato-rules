import tomato as tt
from tomato.functions import utils

from tomato_rules import amoebas as rule

board_dimensions = (100, 100)
cell_size = 4

# Change the last parameter to experiment with different living cell densities. You
# may have to try several times before a matrix that spawns an amoeba is created.
state_matrix = utils.random_int_matrix(board_dimensions, 90)

board = tt.Board(rule, cell_size=cell_size)
board.start(state_matrix)
