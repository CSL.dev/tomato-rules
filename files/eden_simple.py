import tomato as tt
from tomato.functions import utils
from tomato_rules import eden_simple as rule

cell_size = 4
dimensions = 100

state_matrix = utils.cross_matrix(dimensions, 1)

board = tt.Board(rule, cell_size=cell_size)
board.start(state_matrix)
