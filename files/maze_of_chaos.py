import numpy as np
import tomato as tt

from tomato_rules import maze_of_chaos as rule

board_dimensions = (120, 120)

cell_size = 4

# Probabilidades de uma célula morta ou viva (respectivamente) serem selecionadas
# em cada elemento da matriz de estados iniciais. Tente variar estes parâmetros.
prob_dead = 0.96
prob_live = (1 - prob_dead) / 3

state_matrix = np.random.choice(
    (0, 1, 2, 3), size=board_dimensions, p=(prob_dead, prob_live, prob_live, prob_live)
)

# Como a matriz inicial é aleatória, recomendo executar esta célula várias vezes
# para um mesmo conjunto de parâmetros
board = tt.Board(rule, cell_size=cell_size)
board.start(state_matrix)
