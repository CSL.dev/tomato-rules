import tomato as tt
from tomato.functions import utils
from tomato_rules import langtons_ant_old as rule

cell_size = 4
dimensions = (100, 100)
ant_args = {"pos": (100 // 2, 100 // 2), "dir": (1, 0)}

# The cross matrix isn't really necessary for the
# rule to work, I just thought the ant formed a cooler pattern this way
state_matrix = utils.cross_matrix(dimensions, 2)

board = tt.Board(rule, cell_size=cell_size, debug=False, max_fps=300)
board.start(state_matrix, cell_args=ant_args)
