import tomato as tt
from tomato.functions import utils

from tomato_rules import totalistic_automata as rule 

CELL_SIZE = 4
dimensions = (80, 80)
state_matrix = utils.random_int_matrix(dimensions, 2, seed=12)

definitions = {
    "B4/S456L": (255, 0, 255),
}
# definitions = "B3/S23"

board = tt.Board(rule, cell_size=CELL_SIZE)

board.start(state_matrix, cell_args=definitions)
