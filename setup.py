import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="tomato-rules",
    version="1.0",
    author="Eduardo Lopes Dias, Murilo Melhem",
    author_email="eduardosprp@usp.br",
    description="Additional rules for tomato-enfine.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://codeberg.org/eduardotogpi/tomato-rules",
    project_urls={
        "Bug Tracker": "https://codeberg.org/eduardotogpi/tomato-rules/issues",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
        "Operating System :: OS Independent",
    ],
    install_requires=[
        'tomato-engine',
    ],
    package_dir={"": "files"},
    python_requires=">=3.6",
)
