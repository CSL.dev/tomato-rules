# tomato-rules

This is a public repository for sharing rulesets written for
[tomato-engine](https://codeberg.org/eduardotogpi/tomato-engine).
Everyone is encouraged to share any ruleset they've written (see
**Sharing rulesets** below).

## Installation and usage

You can install this package via pip:
```
pip3 install git+https://codeberg.org/CSL.dev/tomato-rules.git
```
And import tomato_rules to use any of the rulesets. For instance,
`high_life`:
```
import tomato as tt
from tomato_rules import high_life

board = tt.Board(high_life)
```

## Sharing rulesets

Given that tomato-engine is free software, you might as
well share any ruleset you've written with everyone through this
repository, right?

All you have to do is put your ruleset on files/tomato-rules/, and
write a simple, minimal working example on files/. You can then
share it with us by sending a pull request, or, send me your
example and ruleset through email (eduardosprp@protonmail.com) if
you prefer.

files/README.md and files/tomato-rules/README.md are helpful
guidelines for writing such examples and rulesets respectively.

## Please note:

Every file in this repository is licensed under the GPLv3+
license, unless specified otherwise, by the author, in a header
on each individual file they would prefer to license differently.

Of course, whatever license you choose for your contributions
[must be compatible with the GPLv3](https://www.gnu.org/licenses/license-list.html#GPLCompatibleLicenses).
GPL-compatible licenses include pretty much any Free
Software/Open Source license commonly in use, so you generally
shouldn't worry about that.
